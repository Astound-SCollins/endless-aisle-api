# Digital Store - API Server Code #
Digital Store Solution is a Demandware in-store reference app, an Appcelerator Mobile application written to enable brick-and-mortar stores to leverage the Demandware platform for in-store sales. 

More information on getting started is in [Getting Started with DSS development](https://xchange.demandware.com/docs/DOC-23020).

This document describes how to get the latest version of the Digital Store Solution server code.

1. Fork the 'master' branch to your account.
2. Clone the account to your laptop.
Note: this automatically sets the alias to 'origin'.
On the command line, this can be accomplished by:
git clone https://<YOUR_ACCOUNT_HERE>@bitbucket.org/<YOUR_ACCOUNT_HERE>/digital-store-api
3. Set up the ability to pull updates from the digital-store-api repo.  On the command line, this can be accomplished by running the following in the digital-store-api directory created in the above step. Be sure to cd into digital-store-api before running.
git remote add upstream https://<YOUR_ACCOUNT_HERE>@bitbucket.org/demandware/digital-store-api

* Only the 'master' branch is available in the digital-store-api repository. For your own code authoring purposes, you might want to create other branches.
* When you want to incorporate the latest version of Digital Store Solution, you can pull into a new branch in your local repository. You then resolve conflicts and test the merged code before merging into your main branch.

* git checkout -b <new-branch>  
You are now in the new branch in your forked project.

* git pull upstream master  
This pulls the latest DSS code into your new branch. You can now resolve conflicts and test the new merged code.