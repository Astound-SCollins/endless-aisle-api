/**
 * ©2013-2017 salesforce.com, inc. All rights reserved.
 * 
 * basketPatch.ds
 * 
 * apply a patch to a basket
 */

importPackage(dw.system);
importPackage(dw.web);
importPackage(dw.value);
importPackage(dw.util);
importPackage(dw.order);
importPackage(dw.campaign);
importPackage(dw.catalog);
importPackage(dw.customer);
importScript("int_ocapi_ext_core:api/EABasket.ds");
importScript("int_ocapi_ext_core:actions/GetCoreCartridgePath.ds");

var shippingPriceOverride = require("./shippingPriceOverride");
var getShippingMethods = require("./getShippingMethods");
var productPriceOverride = require("./productPriceOverride");
var updateProductLineItem = require("./updateProductLineItem");
var Status = require('dw/system/Status');

/**
 * beforePatch
 * 
 * called before patching a basket
 */
exports.beforePatch = function(basket, update) {
    if (update.c_patchInfo) {
        var patch = JSON.parse(update.c_patchInfo);
        var type = patch.type;
        var data = patch.data;
        // see what kind of patch this is
        if (type === "product_price_override") {
            return applyProductPriceOverride(basket, data);
        } else if (type === "basket_replace") {
            return replaceBasket(basket, data);
        } else if (type === "after_abandon_order") {
            // reset the channel type of the basket to DSS
            basket.setChannelType(LineItemCtnr.CHANNEL_TYPE_DSS);
            if(data.currencyCode){
                basket.currencyCode = data.currencyCode;
            }
            if (data.gift_message) {
                return setGiftMessage(basket, data.gift_message);
            }
            return new Status(Status.OK);
        } else {
            return new Status(Status.OK);
        }
    } else {
        return new Status(Status.OK);
    }
};

/**
 * afterPatch
 * 
 * after patching a basket
 */
exports.afterPatch = function(basket, update) {
    if (update.c_patchInfo) {
        var patch = JSON.parse(update.c_patchInfo);
        var type = patch.type;
        var data = patch.data;
        if (type === "validate_cart_for_checkout") {
            dw.system.HookMgr.callHook("dw.ocapi.shop.basket.calculate", "calculate", basket);
            var validate = require(getCoreCartridgePath() + "/cartridge/scripts/cart/ValidateCartForCheckout");
            var pdict = {
                Basket : basket,
                ValidateTax : false
            };
            validate.execute(pdict);
            var enable_checkout = true;
            if (empty(basket) || basket.allProductLineItems.size() == 0) {
                enable_checkout = false;
            }
            if (pdict.EnableCheckout === false) {
                enable_checkout = false;
            }
            var obj = basket.custom.eaCustomAttributes ? JSON.parse(basket.custom.eaCustomAttributes) : {};
            obj.enable_checkout = enable_checkout;
            basket.custom.eaCustomAttributes = JSON.stringify(obj);
            return new Status(Status.OK);
        } else if (type == "get_shipping_method_list") {
            return getShippingMethods.calculateShippingMethods(basket);
        } else if (type == "after_abandon_order"){
        	// We have to put shipments back in afterPATCH because if we put in beforePATCH , it gets wiped out by regular ocapi processing.
            //We are setting the currency in the basket in beforePATCH so that the basket is in the right currency as the shipping method
        	// setting the shipping address back
            for(var i=0; i<data.shipments.length; i++){
                var shippingAddress = basket.shipments[i].createShippingAddress();
                var shipmentObj = data.shipments[i];
                var orderAddress = basket.shipments[i].shippingAddress;
                orderAddress.setFirstName(shipmentObj.shipping_address.first_name);
                orderAddress.setLastName(shipmentObj.shipping_address.last_name);
                orderAddress.setAddress1(shipmentObj.shipping_address.address1);
                if(shipmentObj.shipping_address.address2){	
                    orderAddress.setAddress2(shipmentObj.shipping_address.address2);
                }
                orderAddress.setCity(shipmentObj.shipping_address.city);
                orderAddress.setStateCode(shipmentObj.shipping_address.state_code);
                orderAddress.setCountryCode(shipmentObj.shipping_address.country_code);
                orderAddress.setPostalCode(shipmentObj.shipping_address.postal_code);
                orderAddress.setPhone(shipmentObj.shipping_address.phone);
                
                // setting the shipping method back
                if (data.shipments[i].shipping_method) {
                    var shippingMethod = data.shipments[i].shipping_method.id;
                    var applicableShippingMethods = ShippingMgr.getShipmentShippingModel(basket.shipments[i]).getApplicableShippingMethods();
                    var smIter = applicableShippingMethods.iterator();
                    while (smIter.hasNext()) {
                        var thisMethod = smIter.next();
                        if (thisMethod.getID() == shippingMethod) {
                            basket.shipments[i].setShippingMethod(thisMethod);
                            break;
                        }
                    }
            
                }
            
            // setting the shipping override back
                if (data.shipping_override) {
                    data.shipping_override.ignore_permissions = true;
                    var status = shippingPriceOverride
                            .setShippingPriceOverride(basket, {
                                c_patchInfo : {
                                    type : "shipping_price_override",
                                    data : data.shipping_override
                                }
                            });
                    // if this fails, don't continue
                    if (status.status == Status.ERROR) {
                        return status;
                    }
                }                          
            }
            // make sure to call the calculate hook
            dw.system.HookMgr.callHook( "dw.ocapi.shop.basket.calculate", "calculate", basket );  
            return new Status(Status.OK);
        } else {
            return new Status(Status.OK);
        }
    } else {
        return new Status(Status.OK);
    }
};

/**
 * applyProductPriceOverride
 * 
 * add a price override to a product
 */
function applyProductPriceOverride(basket, overrideData) {
    overrideData.Basket = basket;
    return productPriceOverride.applyProductPriceOverride(overrideData);
}

/**
 * replaceBasket
 * 
 * replace the contents of the basket (products and shipments) with the data sent in
 */
function replaceBasket(basket, data) {
    var product_items = data.product_items;
    var shipments = data.shipments;
    // clear the existing basket
    var result = clearBasket(basket, product_items, data.c_employee_id);
    if (!result) {
        return new Status(Status.ERROR);
    }
    // replace the basket with the data sent in
    return doBasketReplace(basket, product_items, shipments);
}

/**
 * clearBasket
 * 
 * remove the products and coupons from the basket
 */
function clearBasket(basket, product_items, employeeId) {
    var log = Logger.getLogger("instore-audit-trail");
    log.info("ClearBasket: entering script");

    if (empty(basket)) {
        log.info("ClearBasket: exiting script with EA_BASKET_404 error");
        return false;
    }

    log.info("ClearBasket: associate " + employeeId + " is clearing basket");

    // Clear out web products if there are app products to replace them with
    if (product_items) {
        var products = basket.getAllProductLineItems();
        var prodIter = products.iterator();
        while (prodIter.hasNext()) {
            var pli = prodIter.next();
            basket.removeProductLineItem(pli);
        }
    }

    // Clear out all web coupons always
    var coupons = basket.getCouponLineItems();
    var coupIter = coupons.iterator();
    while (coupIter.hasNext()) {
        var couponLineItem = coupIter.next();
        basket.removeCouponLineItem(couponLineItem);
    }

    log.info("ClearBasket: exiting script without error");
    return true;
}

/**
 * doBasketReplace
 * 
 * replace the products and shipments in the basket with the passed in products and shipments
 */
function doBasketReplace(basket, products, shipments) {
    var log = Logger.getLogger("instore-audit-trail");
    log.info("ReplaceBasket: entering script");

    if (empty(basket)) {
        log.info("ReplaceBasket: exiting script with EA_BASKET_404 error");
        return new Status(Status.ERROR);
    }

    // product_items
    if (products) {
        for (var i = 0; i < products.length; i++) {
            var productObj = products[i];
            var product = ProductMgr.getProduct(productObj.product_id);
            var option_items = productObj.option_items;
            var pli = null;
            if (option_items && option_items.length > 0) {
                var option_model = product.getOptionModel();
                updateProductLineItem.updateOptionModel(option_model, option_items);
                pli = basket.createProductLineItem(product, option_model, basket.shipments[0]);
                pli.updateOptionValue(updateProductLineItem.getProductOptionValue(option_model, option_items));

            } else {
                pli = basket.createProductLineItem(productObj.product_id, basket.shipments[0]);
            }
            pli.setQuantityValue(productObj.quantity);

            var obj = pli.custom.eaCustomAttributes ? JSON.parse(pli.custom.eaCustomAttributes) : {};
            pli.custom.eaPriceOverrideType = "none";
            delete obj.eaManagerEmployeeId;
            delete obj.eaPriceOverrideType;
            delete obj.eaPriceOverrideValue;
            delete obj.eaOverrideReasonCode;
            pli.custom.eaCustomAttributes = JSON.stringify(obj);
        }
    }

    // shipment - since we can't delete the default shipment, let's overwrite it
    // with new new data.
    if (shipments) {
        var shipment = basket.shipments[0];

        for (var i = 0; i < shipments.length; i++) {
            var shipmentObj = shipments[i];

            // First replace the shipping address (if one is supplied)
            if (shipmentObj.shipping_address) {
                var orderAddress = shipment.shippingAddress ? shipment.shippingAddress : shipment.createShippingAddress();
                orderAddress.setFirstName(shipmentObj.shipping_address.first_name);
                orderAddress.setLastName(shipmentObj.shipping_address.last_name);
                orderAddress.setAddress1(shipmentObj.shipping_address.address1);
                orderAddress.setAddress2(shipmentObj.shipping_address.address2);
                orderAddress.setCity(shipmentObj.shipping_address.city);
                orderAddress.setStateCode(shipmentObj.shipping_address.state_code);
                orderAddress.setCountryCode(shipmentObj.shipping_address.country_code);
                orderAddress.setPostalCode(shipmentObj.shipping_address.postal_code);
                orderAddress.setPhone(shipmentObj.shipping_address.phone);
            }

            // Now, replace the shipping method (if one is supplied)
            if (shipmentObj.shipping_method) {
                var shippingMethod = shipment.shippingMethod ? shipment.shippingMethod : shipment.getShippingMethod();
                if (empty(shippingMethod) || (shippingMethod.getID() != shipmentObj.shipping_method.id)) {
                    var applicableShippingMethods = ShippingMgr.getShipmentShippingModel(shipment).getApplicableShippingMethods();
                    var smIter = applicableShippingMethods.iterator();
                    while (smIter.hasNext()) {
                        var thisMethod = smIter.next();
                        if (thisMethod.getID() == shipmentObj.shipping_method.id) {
                            shipment.setShippingMethod(thisMethod);
                            break;
                        }
                    }
                }
            }

            var slis = shipment.getShippingLineItems();
            var sli = slis ? slis[0] : shipment.createShippingLineItem("new_shipping_line_item");

            var obj = sli.custom.eaCustomAttributes ? JSON.parse(sli.custom.eaCustomAttributes) : {};

            sli.custom.eaPriceOverrideType = "none";
            obj.price_override = false;
            delete obj.eaManagerEmployeeId;
            delete obj.eaPriceOverrideType;
            delete obj.eaPriceOverrideValue;
            delete obj.eaOverrideReasonCode;
            sli.custom.eaCustomAttributes = JSON.stringify(obj);

        }
    }
    log.info("ReplaceBasket: exiting script without error");
    return new Status(Status.OK);
}

/**
 * setGiftMessage
 * 
 * set the gift message in the basket
 */
function setGiftMessage(basket, data) {
    var log = Logger.getLogger("instore-audit-trail");
    log.info("basketPatch setGiftMessage setting gift info");
    var isGift = data.is_gift;
    var message = data.message;
    var shipments = basket.getShipments();
    var sIterator = shipments.iterator();
    while (sIterator.hasNext()) {
        var shipment = sIterator.next();
        if (isGift == "true") {
            shipment.setGift(true);
            shipment.setGiftMessage(message);
        } else {
            shipment.setGift(false);
            shipment.setGiftMessage(message);
        }
    }
    log.info("basketPatch setGiftMessage exit");
    return new Status(Status.OK);
}