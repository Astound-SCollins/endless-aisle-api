'use strict';

/**
 * ©2013-2017 salesforce.com, inc. All rights reserved.
 * 
 * @module controllers/Verifone
 */

/* API Includes */
var ISML = require('dw/template/ISML');

/* Script Modules */
var guard = require('app_storefront_controllers/cartridge/scripts/guard');
var EAUtils = require('int_ocapi_ext_controllers/cartridge/controllers/EAUtils');
var DecryptTrackData = require('int_verifone_dss_core/cartridge/scripts/verifone/DecryptTrackData');
var DecryptCardData = require('int_verifone_dss_core/cartridge/scripts/verifone/DecryptCardData');
var IsDeviceEnabled = require('int_verifone_dss_core/cartridge/scripts/verifone/IsDeviceEnabled');

function DecryptTrackData(requestObject) {
    var args = {
        DeviceID : requestObject.terminal_id,
        T1 : requestObject.track_1,
        T2 : requestObject.track_2
    }
    var result = DecryptTrackData.execute(args);
    return result;
}

function DecryptCardData(requestObject) {
    var args = {
        DeviceID : requestObject.terminal_id,
        exp : requestObject.expire_date,
        pan : requestObject.pan
    }
    var result = DecryptCardData.execute(args);
    return result;
}

function ActivateDevice() {
    var JSONResponse;
    var CurrentHTTPParameterMap = request.httpParameterMap;
    DecryptTrackData.decryptTrackData(CurrentHTTPParameterMap.track_1.value, CurrentHTTPParameterMap.track_2.value, CurrentHTTPParameterMap.terminal_id.value);
    var result = IsDeviceEnabled.isDeviceEnabled(args.DecryptedData);
    if (result.success) {
        JSONResponse = {
            "httpStatus" : "200"
        };
    } else {
        JSONResponse = {
            "httpStatus" : "400"
        };
    }
    ISML.renderTemplate('responses/json', {
        JSONResponse : JSONResponse
    });
}

function DecryptCreditCard(requestObject) {
    if (requestObject.terminal_id != null) {
        if (RequestObject.track_1 != null) {
            var result = DecryptTrackData(requestObject);
            if (result.error) {
                return {
                    error : true
                };
            } else {
                requestObject.track_1 = DecryptedT1;
                requestObject.track_2 = DecryptedT2;
            }
        } else {
            var result = DecryptCardData(requestObject);
            if (result.error) {
                return {
                    error : true
                };
            } else {
                requestObject.pan = DecryptedPan;
                requestObject.expire_date = DecryptedExp;
            }
        }
    }
}

/*
 * Public Methods
 */
exports.DecryptTrackData = DecryptTrackData;
exports.DecryptCardData = DecryptCardData;
exports.DecryptCreditCard = DecryptCreditCard;

/*
 * Web Exposed Methods
 */
exports.ActivateDevice = guard.ensure([ 'https', 'post' ], ActivateDevice);
